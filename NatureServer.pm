package Net::Nature::Server;
use Net::SSH::Perl::Subsystem::Server;
use base qw( Net::SSH::Perl::Subsystem::Server );
use File::System;
use Redis;
use Digest::Keccak qw(keccak_256 keccak_256_hex keccak_256_base64);
use strict;
use Net::SSH::Perl::Buffer;

our $redis = undef;
our %filesystems;

use constant MSG_NEW_USER => 1;

sub init {
    my $ss = shift;
    $ss->SUPER::init(@_);
    
    $ss->register_handler(MSG_NEW_USER,\&new_user);
}

sub new_user {
    my $ss = shift;
    my($msg) = @_;
    my $name = $msg->get_string;
    my $password = $msg->get_string;
    if ($redis->exists(chomp($name))){
        print "User with that name exists.";
        my $err = Net::SSH::Perl::Buffer->new();
        $err->put_str('User with that name exists.');
        $msg->send_msg($err);
        return;
    }
    else
    {
        eval {
            $filesystems[keccak_256_base64($name . keccak_256_hex($password))] = File::System->new('Real','/users/' . keccak_256_base64($name . keccak_256_hex($password)) . '/root/');
            $redis->set(chomp($name), keccak_256_hex($password), 'EX', 'PX', 'NX');
        } or do {
            my $err = Net::SSH::Perl::Buffer->new();
            $err->put_str('Error: can`t create file system.');
            $msg->send_msg($err);
            return;
        }
    }
}

sub StartRedis {
    my $ss = shift;
    my ($password, $server, $name) = @_;
    $redis = Redis->new(password => chomp($password), server => $server, name => chomp($name));
}

1;